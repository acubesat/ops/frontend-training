# Frontend Training

  



## Overview of the training


Welcome to the OPS' frontend training! This training is specifically designed to help you understand and practise some core concepts of angular's framework that you will find useful when developing the OPS' [frontend software](https://gitlab.com/acubesat/ops/frontend-software). These concepts include: 

- File structure in angular projects (components, services, directives).
- Functions and connecting them to HTML elements.
- HTTP requests, observables and subscription concepts.
- NGX graphs.
- YAMCS' interfaces and HTTP requests to YAMCS' API.


## Getting started


Before you start working on the training tasks, it is highly recommended to watch [this video](https://www.youtube.com/watch?v=k5E2AVpwsko), which explains how angular and typescript work. It will help you get a grasp of the concepts and help you understand what you'll need to do to solve the tasks. Make sure to follow the video's instructions to install npm and angular's CLI. After you have finished watching the video, you can then move on to solving!


### Training instructions


In this section there are the necessary instructions that you'll need to follow to create your project and upload your solutions. Please follow them **carefully**.

- Create a **new branch from the main branch** in the [frontend training](https://gitlab.com/acubesat/ops/frontend-training) repository that will be named **master-yourName** (e.g. master-john)
- Follow the instructions of each task to complete it. Start from the [first task](https://gitlab.com/acubesat/ops/frontend-training/-/issues/6) and work your way up to the rest. For each task, create a new branch from your master branch named **yourName-taskNumberOfTask**. For example, if you're solving the second task, your branch should be named john-task2.
- When you have completed a task create a merge request from the task's branch to your master branch and wait for it to be reviewed and merged before you continue with the rest of the tasks. 

Please note that when you upload the code to a task, you can have multiple commits, but make sure that when the task is **fully complete**, the commit's name to be **taskNumberOfTask final implementation**. 


### Links to the tasks


- [Task 1](https://gitlab.com/acubesat/ops/frontend-training/-/issues/6)
- [Task 2](https://gitlab.com/acubesat/ops/frontend-training/-/issues/10)
- [Task 3](https://gitlab.com/acubesat/ops/frontend-training/-/issues/7)
- [Task 4](https://gitlab.com/acubesat/ops/frontend-training/-/issues/9)
- [Task 5](https://gitlab.com/acubesat/ops/frontend-training/-/issues/13)
- [Yamcs Task](https://gitlab.com/acubesat/ops/frontend-training/-/issues/11)
- [Intermediate Task 1](https://gitlab.com/acubesat/ops/frontend-training/-/issues/14)
- [Intermediate Task 2](https://gitlab.com/acubesat/ops/frontend-training/-/issues/15)
- [Intermediate Task 3](https://gitlab.com/acubesat/ops/frontend-training/-/issues/16)
